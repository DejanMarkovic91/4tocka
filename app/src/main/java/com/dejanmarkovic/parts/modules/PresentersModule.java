package com.dejanmarkovic.parts.modules;

import com.dejanmarkovic.parts.features.FirebaseDatabaseFeature;
import com.dejanmarkovic.parts.features.FirebaseMessagingFeature;
import com.dejanmarkovic.parts.features.LoginFeature;
import com.dejanmarkovic.parts.presenters.ChatPresenter;
import com.dejanmarkovic.parts.presenters.OfferPreviewPresenter;
import com.dejanmarkovic.parts.repository.DatabaseRepository;
import com.dejanmarkovic.parts.presenters.ContentLandingPresenter;
import com.dejanmarkovic.parts.presenters.LoginPresenter;
import com.dejanmarkovic.parts.presenters.NewPartPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by dejanmarkovic on 4/11/17.
 */

@Module//(includes = DataRepositoriesModule.class)
public class PresentersModule {

    @Provides
    protected LoginPresenter provideLoginPresenter(LoginFeature loginFeature,
                                                   FirebaseDatabaseFeature firDatabaseFeature,
                                                   FirebaseMessagingFeature firebaseMessaging) {
        return new LoginPresenter(loginFeature, firDatabaseFeature, firebaseMessaging);
    }

    @Provides
    protected ContentLandingPresenter provideContentLandingPresenter(LoginFeature loginFeature,
                                                                     FirebaseDatabaseFeature df) {
        return new ContentLandingPresenter(loginFeature, df);
    }

    @Provides
    protected NewPartPresenter provideOfferPresenter(LoginFeature loginFeature,
                                                     FirebaseDatabaseFeature databaseFeature) {
        return new NewPartPresenter(loginFeature, databaseFeature);
    }

    @Provides
    protected OfferPreviewPresenter provideOfferPreviewPresenter(LoginFeature loginFeature,
                                                                 FirebaseDatabaseFeature databaseFeature) {
        return new OfferPreviewPresenter(loginFeature, databaseFeature);
    }

    @Provides
    protected ChatPresenter provideChatPresenter(LoginFeature loginFeature,
                                                 FirebaseDatabaseFeature databaseFeature) {
        return new ChatPresenter(loginFeature, databaseFeature);
    }
}
