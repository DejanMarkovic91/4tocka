package com.dejanmarkovic.parts.modules;

import android.content.Context;

import com.dejanmarkovic.parts.PartsApplication;
import com.dejanmarkovic.parts.repository.DatabaseRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataRepositoriesModule {

    private Context context;

    public DataRepositoriesModule(PartsApplication application) {
        this.context = application.getApplicationContext();
    }

    @Provides
    @Singleton
    DatabaseRepository provideDatabaseFeature() {
        return new DatabaseRepository(context);
    }

}
