package com.dejanmarkovic.parts;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.dejanmarkovic.parts.modules.DataRepositoriesModule;
import com.dejanmarkovic.parts.modules.HttpClientModule;

import io.fabric.sdk.android.Fabric;

/**
 * Created by dejanmarkovic on 4/10/17.
 */

public class PartsApplication extends Application {

    protected PartsApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        if (PartsBuildProcedure.isAnalyzerProcess(this)) {
            //Ovo je poseban proces koji se koristi za analizu heap-a
            return;
        }
        PartsBuildProcedure.buildDebugTools(this);
        buildApplicationComponent();
    }

    protected void buildApplicationComponent() {
        applicationComponent = DaggerPartsApplicationComponent.builder()
                //.globalEnvironmentModule(new GlobalEnvironmentModule(this))
                //.databaseModule(new DatabaseModule(this))
                //.analyticsModule(new AnalyticsModule(this))
                .httpClientModule(new HttpClientModule())
                .dataRepositoriesModule(new DataRepositoriesModule(this))
                .build();
    }

    public PartsApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
