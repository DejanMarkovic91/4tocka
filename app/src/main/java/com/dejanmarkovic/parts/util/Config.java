package com.dejanmarkovic.parts.util;

import com.cocosw.favor.AllFavor;
import com.dejanmarkovic.parts.repository.entities.Models;

import java.util.ArrayList;

/**
 * Created by dejanmarkovic on 4/15/17.
 */

@AllFavor
public interface Config {

    void setCategories(ArrayList<Models> categories);

    ArrayList<Models> getCategories();

}
