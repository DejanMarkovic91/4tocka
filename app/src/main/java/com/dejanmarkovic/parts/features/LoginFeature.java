package com.dejanmarkovic.parts.features;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import com.dejanmarkovic.parts.repository.entities.Company;
import com.dejanmarkovic.parts.repository.entities.ImageDescriptor;
import com.dejanmarkovic.parts.repository.entities.UserDetails;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseAuth;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by dejanmarkovic on 4/11/17.
 */

public class LoginFeature {

    @Inject
    public LoginFeature() {

    }

    public Observable<AuthResult> authenticate(AuthCredential credential) {
        return RxFirebaseAuth
                .signInWithCredential(FirebaseAuth.getInstance(), credential)
                .observeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public FirebaseUser getCurrentUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    public void logout() {
        FirebaseAuth.getInstance().signOut();
    }

    public Observable<AuthResult> registerUser(String email, String password) {
        return RxFirebaseAuth.createUserWithEmailAndPassword(FirebaseAuth.getInstance(),
                email,
                password)
                .observeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void populateUserAdditionalData(String name,
                                           Company company,
                                           String city,
                                           String cityCode,
                                           String address,
                                           String phone) {
        DatabaseReference reference = FirebaseDatabase
                .getInstance()
                .getReference(getCurrentUser().getUid());
        UserDetails details = new UserDetails();
        details.setNameSurname(name);
        details.setCity(city);
        details.setCityCode(cityCode);
        details.setAddress(address);
        details.setPhone(phone);
        details.setCompany(company);
        reference.setValue(details);
        reference.push();
    }

    public Observable<UserDetails> getUserDetails() {
        return RxFirebaseDatabase.observeValueEvent(FirebaseDatabase
                .getInstance()
                .getReference(getCurrentUser().getUid()), DataSnapshotMapper.of(UserDetails.class))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void addImageDescriptorToUser(FirebaseDatabase database,
                                         List<ImageDescriptor> descriptors) {
        database.getReference(getCurrentUser().getUid()).child("images").setValue(descriptors);
    }

}
