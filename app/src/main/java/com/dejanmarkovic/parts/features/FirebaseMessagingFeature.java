package com.dejanmarkovic.parts.features;

import com.dejanmarkovic.parts.repository.DatabaseRepository;
import com.dejanmarkovic.parts.repository.entities.SendMessage;
import com.dejanmarkovic.parts.repository.entities.TopicEntity;
import com.dejanmarkovic.parts.services.messages.ReceivedMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dejanmarkovic on 4/26/17.
 */

public class FirebaseMessagingFeature {


    @Inject
    DatabaseRepository databaseRepository;

    @Inject
    public FirebaseMessagingFeature() {

    }

    public void subscribe(String topic) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic);
        saveTopic(topic, TopicEntity.SUBSCRIBED);
    }

    public void subscribeListOfTopics(List<String> topics) {
        for (String topic : topics) FirebaseMessaging.getInstance().subscribeToTopic(topic);
        saveTopics(topics, TopicEntity.SUBSCRIBED);
    }

    public void unsubscribe(String topic) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
        saveTopic(topic, TopicEntity.UNSUBSCRIBED);
    }

    public void unsubscribeListOfTopics(List<String> topics) {
        for (String topic : topics) FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
        saveTopics(topics, TopicEntity.UNSUBSCRIBED);
    }

    private void saveTopic(String topic, String status) {
        TopicEntity entity = new TopicEntity();
        entity.setStatus(status);
        entity.setTopicName(topic);
        databaseRepository.saveEntity(entity);
    }

    private void saveTopics(List<String> topics, String status) {
        List<TopicEntity> entities = new ArrayList<>();
        for (String topic : topics) {
            TopicEntity entity = new TopicEntity();
            entity.setTopicName(topic);
            entity.setStatus(status);
            entities.add(entity);
        }
        databaseRepository.saveEntities(entities);
    }

    public void sendPartMessage(String topic, String partId) {
        //send to myself
        FirebaseMessaging.getInstance().subscribeToTopic(topic);
        HashMap<String, String> data = new HashMap<>();
        data.put(ReceivedMessage.TYPE, String.valueOf(ReceivedMessage.OFFER_MESSAGE));
        data.put(ReceivedMessage.PART_ID, partId);
        Observable.create(subscriber -> {
            try {
                subscriber.onNext(sendMessage(topic, data, 1800));
            } catch (Exception e) {
                subscriber.onError(e);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        code -> System.out.println("ReceivedMessage sent: " + code.toString()),
                        throwable -> throwable.printStackTrace());
    }

    public int sendMessage(String to, HashMap<String, String> data, long ttlSeconds)
            throws Exception {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setTo("/topics/" + to);
        sendMessage.setData(data);
        sendMessage.setTime_to_live(ttlSeconds);
        String messageBytes = new ObjectMapper().writeValueAsString(sendMessage);

        URL url = new URL("https://fcm.googleapis.com/fcm/send");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/json");
        urlConnection.setRequestProperty("Authorization", "key=AIzaSyChkOSS8qOX7YglD909nEErWzTCX0t0ahw");

        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
        wr.writeBytes(messageBytes);
        wr.flush();
        wr.close();
        urlConnection.connect();

        return urlConnection.getResponseCode();
    }
}
