package com.dejanmarkovic.parts.features;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;

import com.dejanmarkovic.parts.activities.adapters.items.PartHeaderItem;
import com.dejanmarkovic.parts.activities.adapters.items.PartItem;
import com.dejanmarkovic.parts.repository.entities.Image;
import com.dejanmarkovic.parts.repository.entities.Models;
import com.dejanmarkovic.parts.repository.entities.Part;
import com.dejanmarkovic.parts.repository.entities.PartDescriptor;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kelvinapps.rxfirebase.DataSnapshotMapper;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;
import com.kelvinapps.rxfirebase.RxFirebaseStorage;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by dejanmarkovic on 4/12/17.
 */

public class FirebaseDatabaseFeature {

    @Inject
    LoginFeature loginFeature;
    @Inject
    FirebaseMessagingFeature messagingFeature;

    @Inject
    public FirebaseDatabaseFeature() {

    }

    public Observable<List<Models>> getCarCategoriesObservable() {
        return RxFirebaseDatabase.observeSingleValueEvent(FirebaseDatabase
                        .getInstance()
                        .getReference()
                        .child("public")
                        .child("car_categories"),
                DataSnapshotMapper.listOf(Models.class))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UploadTask.TaskSnapshot> saveImage(StorageReference firebaseDatabase,
                                                         byte[] imageByteData) {
        return RxFirebaseStorage.putBytes(firebaseDatabase, imageByteData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public boolean savePart(Context context, String category, String model,
                            String part, ArrayList<Parcelable> images, Part data) {
        try {
            DatabaseReference reference = FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child("parts")
                    .child(category)
                    .child(model)
                    .child(part);

            ArrayList<Image> imagesForPart = new ArrayList<>();

            //save images
            for (Parcelable p : images) {
                Image image = new Image();
                Uri uri = (Uri) p;
                String url = uri.getPath().toString();
                String imageName = UUID.randomUUID() + "_" + url.substring(url.lastIndexOf('/') + 1);

                image.setId(imageName);
                image.setPath(uri.toString());

                Bitmap src = MediaStore.Images.Media.getBitmap(
                        context.getContentResolver(), uri);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                src.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] byteData = baos.toByteArray();
                StorageReference firebaseDatabase = FirebaseStorage.getInstance()
                        .getReference().child("pictures").child(imageName);
                saveImage(firebaseDatabase, byteData)
                        .subscribe(taskSnapshot -> {
                                },
                                throwable -> {
                                    imagesForPart.remove(image);
                                    throwable.printStackTrace();
                                });
            }

            //save part for part companies
            data.setUserID(loginFeature.getCurrentUser().getUid());
            data.setImages(imagesForPart);
            LinkedHashMap<String, Object> map = new LinkedHashMap<>();
            String id = UUID.randomUUID().toString();
            data.setId(id);
            String messageTopic = category + "%" + model;
            String path = "parts@" + category + "@" + model + "@" + part + "@" + id;
            messagingFeature.subscribe(messageTopic);
            messagingFeature.sendPartMessage(messageTopic, id);
            data.setPath(path);
            map.put(id, data);
            reference.updateChildren(map);
            reference.push();

            //update parts for user
            DatabaseReference reference1 = FirebaseDatabase
                    .getInstance()
                    .getReference(loginFeature.getCurrentUser().getUid())
                    .child("parts");

            PartDescriptor desc = new PartDescriptor();
            desc.setPath(path);
            desc.setTimeCreated(data.getTimeCreated());
            desc.setTimeRemaining(data.getTimeToLive());

            LinkedHashMap<String, Object> partDescriptorData = new LinkedHashMap<>();
            partDescriptorData.put(UUID.randomUUID().toString(), desc);
            reference1.updateChildren(partDescriptorData);
            reference1.push();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Observable<List<PartItem>> getPartsForModelAndCategory(String model, String category) {
        return RxFirebaseDatabase.observeSingleValueEvent(
                FirebaseDatabase
                        .getInstance()
                        .getReference()
                        .child("parts")
                        .child(model)
                        .child(category), DataSnapshotMapper.listOf(LinkedHashMap.class))
                .map(new Func1<List<LinkedHashMap>, List<PartItem>>() {
                    @Override
                    public List<PartItem> call(List<LinkedHashMap> linkedHashMaps) {
                        return null;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<PartItem>> getMyPartsRequests() {
        DatabaseReference reference = FirebaseDatabase
                .getInstance()
                .getReference(loginFeature.getCurrentUser().getUid())
                .child("parts");
        return RxFirebaseDatabase.observeSingleValueEvent(reference,
                DataSnapshotMapper.listOf(PartDescriptor.class))
                .map(partDescriptors -> {
                    List<PartItem> items = new ArrayList<>();
                    for (PartDescriptor pd : partDescriptors) {
                        items.add(new PartHeaderItem(pd));
                    }
                    return items;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Part> loadPartDetails(String path) {
        DatabaseReference reference = FirebaseDatabase
                .getInstance()
                .getReference(path);
        return RxFirebaseDatabase.observeSingleValueEvent(reference,
                DataSnapshotMapper.of(Part.class))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
