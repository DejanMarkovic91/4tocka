package com.dejanmarkovic.parts;

import com.dejanmarkovic.parts.activities.ChatActivity;
import com.dejanmarkovic.parts.activities.ContentLandingActivity;
import com.dejanmarkovic.parts.activities.LoginActivity;
import com.dejanmarkovic.parts.activities.NewPartActivity;
import com.dejanmarkovic.parts.activities.OfferPreviewActivity;
import com.dejanmarkovic.parts.modules.DataRepositoriesModule;
import com.dejanmarkovic.parts.modules.HttpClientModule;
import com.dejanmarkovic.parts.modules.PresentersModule;

import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by dejanmarkovic on 4/10/17.
 */

@Singleton
@Component(modules = {
        HttpClientModule.class,
        DataRepositoriesModule.class,
        PresentersModule.class
})
public interface PartsApplicationComponent {

    void inject(LoginActivity activity);

    void inject(ContentLandingActivity activity);

    void inject(NewPartActivity newPartActivity);

    void inject(OfferPreviewActivity offerPreviewActivity);

    void inject(ChatActivity chatActivity);
}
