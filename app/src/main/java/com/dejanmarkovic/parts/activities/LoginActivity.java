package com.dejanmarkovic.parts.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.os.ResultReceiver;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.dejanmarkovic.parts.Manifest;
import com.dejanmarkovic.parts.PartsApplication;
import com.dejanmarkovic.parts.R;
import com.dejanmarkovic.parts.presenters.LoginPresenter;
import com.dejanmarkovic.parts.repository.entities.Models;
import com.dejanmarkovic.parts.services.FetchAddressIntentService;
import com.dejanmarkovic.parts.util.SpinnerDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoginPresenter.View,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String RETURN_CLASS = "RETURN_CLASS";
    private static final int LOCATION_REQUEST = 12;
    // UI references.
    @BindView(R.id.email)
    AutoCompleteTextView mEmailView;
    @BindView(R.id.password)
    EditText mPasswordView;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.registration_part)
    View registrationPart;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.company_check)
    AppCompatCheckBox companyCheck;
    @BindView(R.id.city)
    EditText city;
    @BindView(R.id.city_code)
    EditText cityCode;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.contact_phone)
    EditText contactPhone;
    @BindView(R.id.submit)
    Button submit;
    private Class returnClass;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private boolean mAddressRequested;
    private ArrayList<Models> companySupportedModels = new ArrayList<>();

    @OnClick(R.id.automatic_locate)
    public void locateMe() {

    }

    private int mode = 0;// 0 - login, 1 - registration

    @Inject
    LoginPresenter presenter;

    private SpinnerDialog spinner;

    @OnClick(R.id.submit)
    void login() {
        if (mode == 0) attemptLogin();
        else attemptRegistration();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ((PartsApplication) getApplication()).getApplicationComponent().inject(this);
        ButterKnife.bind(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        spinner = new SpinnerDialog(this);
        this.returnClass = (Class) getIntent().getSerializableExtra(RETURN_CLASS);

        //setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle(R.string.title_activity_login);

        mEmailView.setText("dejan@gmail.com");
        mPasswordView.setText("jebiga1");
        //getSupportActionBar().setTitle(R.string.title_activity_login);
        // Set up the login form.

        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mode = Integer.valueOf(tab.getPosition());
                updateViews();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        updateViews();
    }

    private void updateViews() {
        if (mode == 0) {
            //login
            registrationPart.setVisibility(View.GONE);
            submit.setText(R.string.login);

        } else {
            //register
            registrationPart.setVisibility(View.VISIBLE);
            submit.setText(R.string.register);

        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        resetErrors();

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        presenter.attemptLogin(email, password);
    }

    private void attemptRegistration() {
        resetErrors();

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String nameValue = name.getText().toString();
        String cityNameValue = city.getText().toString();
        String cityCodeValue = cityCode.getText().toString();
        String addressValue = address.getText().toString();
        String phoneValue = contactPhone.getText().toString();


        presenter.attemptRegistration(
                nameValue,
                companyCheck.isChecked(),
                cityNameValue,
                cityCodeValue,
                addressValue,
                phoneValue,
                email,
                password);
    }

    private void resetErrors() {
        mEmailView.setError(null);
        mPasswordView.setError(null);
        name.setError(null);
        city.setError(null);
        cityCode.setError(null);
        address.setError(null);
        contactPhone.setError(null);
    }

    @Override
    public void showPasswordInvalidInfo() {
        mPasswordView.setError(getString(R.string.error_invalid_password));
        mPasswordView.requestFocus();
    }

    @Override
    public void showEmailInvalidInfo() {
        mEmailView.setError(getString(R.string.error_invalid_email));
        mEmailView.requestFocus();
    }

    @Override
    public void showNameEmptyError() {
        name.setError(getString(R.string.error_field_required));
        name.requestFocus();
    }

    @Override
    public void showCityEmptyError() {
        city.setError(getString(R.string.error_field_required));
        city.requestFocus();
    }

    @Override
    public void showCityCodeEmptyError() {
        cityCode.setError(getString(R.string.error_field_required));
        cityCode.requestFocus();
    }

    @Override
    public void showAddressEmptyError() {
        address.setError(getString(R.string.error_field_required));
        address.requestFocus();
    }

    @Override
    public void showPhoneEmptyError() {
        contactPhone.setError(getString(R.string.error_field_required));
        contactPhone.requestFocus();
    }

    @Override
    public void loginSuccess() {
        ContentLandingActivity.showContent(this);
    }

    @Override
    public void showMessage(String localizedMessage) {
        Toast.makeText(this, localizedMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(boolean show) {
        if (show) {
            spinner.show();
        } else {
            spinner.dismiss();
        }
    }

    @Override
    public void registrationSuccess() {
        if (returnClass != null) {
            startActivity(new Intent(this, returnClass));
        } else {
            ContentLandingActivity.showContent(this);
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.onPause();
        }
        if (spinner != null && spinner.isShowing()) {
            spinner.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onDestroy();
        }
    }

    public static void launchLoginOrRegistration(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    public static void launchLoginOrRegistrationWithReturn(Context context, Class c) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(RETURN_CLASS, c);
        context.startActivity(intent);
    }

    @Override
    public void showModelChooserDialog(ModelSelectorCallback callback) {
        ListView list = (ListView) LayoutInflater.from(this).inflate(R.layout.dialog_models_list, null, false);
        companySupportedModels.clear();
        list.setAdapter(new ArrayAdapter<Models>(this,
                android.R.layout.simple_list_item_checked, presenter.getModels()) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View recyclerView = convertView;
                if (recyclerView == null) {
                    recyclerView = LayoutInflater.from(parent.getContext())
                            .inflate(android.R.layout.simple_list_item_checked, parent, false);
                }
                ((CheckedTextView) recyclerView).setText(getItem(position).getName());
                return recyclerView;
            }
        });
        list.setOnItemClickListener((adapterView, view, i, l) -> {
            if (companySupportedModels.contains(adapterView.getSelectedItem())) {
                companySupportedModels.remove(adapterView.getSelectedItem());
                ((CheckedTextView) view).setChecked(false);
            } else {
                companySupportedModels.add((Models) adapterView.getSelectedItem());
                ((CheckedTextView) view).setChecked(true);
            }
        });
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setView(list).setPositiveButton(android.R.string.ok,
                (dialogInterface, i) -> {
                    if (companySupportedModels.isEmpty()) callback.modelsSelected(null);
                    else callback.modelsSelected(companySupportedModels);//all
                })
                .show();
    }

    @OnClick(R.id.automatic_locate)
    public void fetchAddressButtonHandler() {
        // Only start the service to fetch the address if GoogleApiClient is
        // connected.
        if (mGoogleApiClient.isConnected() && mLastLocation != null) {
            FetchAddressIntentService.startAddressIntentService(this,
                    new AddressResultReceiver(null), mLastLocation);
        } else {
            mGoogleApiClient.connect();
        }
        // If GoogleApiClient isn't connected, process the user's request by
        // setting mAddressRequested to true. Later, when GoogleApiClient connects,
        // launch the service to fetch the address. As far as the user is
        // concerned, pressing the Fetch Address button
        // immediately kicks off the process of getting the address.
        mAddressRequested = true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Gets the best and most recent location currently available,
        // which may be null in rare cases when a location is not available.
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLastLocation != null) {
                // Determine whether a Geocoder is available.
                if (!Geocoder.isPresent()) {
                    Toast.makeText(this, R.string.no_geocoder_available,
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (mAddressRequested) {
                    FetchAddressIntentService.startAddressIntentService(this,
                            new AddressResultReceiver(null), mLastLocation);
                }
            }
        } else {
            if (ContextCompat.checkSelfPermission(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchAddressButtonHandler();
                } else {
                    Toast.makeText(this, R.string.location_not_allowed, Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public interface ModelSelectorCallback {
        void modelsSelected(List<Models> models);
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string
            // or an error message sent from the intent service.
            String mAddressOutput = resultData.getString(FetchAddressIntentService.RESULT_DATA_KEY);

            // Show a toast message if an address was found.
            if (resultCode == FetchAddressIntentService.SUCCESS_RESULT) {
                address.setText(mAddressOutput);
            }

        }
    }
}

