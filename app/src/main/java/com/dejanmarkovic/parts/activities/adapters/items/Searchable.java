package com.dejanmarkovic.parts.activities.adapters.items;

/**
 * Created by dejanm on 27-Apr-17.
 */

public interface Searchable {

    /**
     * Determine if object implementing this interface contains query specified in parameter.
     * Query should be lowercase, to assure better matching.
     *
     * @param query query to be searched in object
     * @return true if contains query, false if don't
     */
    boolean search(String query);

}
