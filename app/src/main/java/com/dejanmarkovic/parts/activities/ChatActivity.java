package com.dejanmarkovic.parts.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.dejanmarkovic.parts.PartsApplication;
import com.dejanmarkovic.parts.R;
import com.dejanmarkovic.parts.presenters.ChatPresenter;
import com.github.bassaer.chatmessageview.views.ChatView;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dejanm on 05-May-17.
 */

public class ChatActivity extends AppCompatActivity implements ChatPresenter.View {
    public static final int READ_REQUEST_CODE = 100;

    @BindView(R.id.chat_view)
    ChatView chatView;

    @Inject
    ChatPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ((PartsApplication) getApplication()).getApplicationComponent().inject(this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onResume(this);
            presenter.initChat(chatView);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onDestroy();
        }
    }

    @Override
    public Activity getContext() {
        return this;
    }

    @Override
    public void requestImages(Intent intent) {
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != READ_REQUEST_CODE || resultCode != RESULT_OK || data == null) {
            return;
        }
        Uri uri = data.getData();
        try {
            Bitmap picture = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            presenter.addImageToChat(chatView, picture);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
        }

    }

    public static void openChat(Context context) {
        context.startActivity(new Intent(context, ChatActivity.class));
    }
}
