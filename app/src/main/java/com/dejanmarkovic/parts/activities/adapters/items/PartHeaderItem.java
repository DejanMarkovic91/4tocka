package com.dejanmarkovic.parts.activities.adapters.items;

import com.dejanmarkovic.parts.repository.entities.PartDescriptor;

import java.util.Calendar;
import java.util.Formatter;
import java.util.Locale;

/**
 * Created by dejanmarkovic on 4/19/17.
 */

public class PartHeaderItem extends PartItem {

    private PartDescriptor part;
    private boolean expanded;

    public PartHeaderItem(PartDescriptor part) {
        super(HEADER);
        this.part = part;
    }

    public PartDescriptor getPart() {
        return part;
    }

    public void setPart(PartDescriptor part) {
        this.part = part;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public String getFormattedTime() {
        long remainingTime = part.getTimeRemaining() - Calendar.getInstance().getTimeInMillis();
        return stringForTime(remainingTime);
    }

    private String stringForTime(long timeMs) {
        if (timeMs < 0) {
            return " (Istekla ponuda)";
        }
        StringBuilder mFormatBuilder = new StringBuilder();
        Formatter mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());

        long totalSeconds = timeMs / 1000;

        long seconds = totalSeconds % 60;
        long minutes = (totalSeconds / 60) % 60;
        long hours = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 24) {
            return mFormatter.format(" (%d d)", hours / 24).toString();
        } else if (hours > 0) {
            return mFormatter.format(" (%d h:%02d m)", hours, minutes).toString();
        } else {
            return mFormatter.format(" (%02d m:%02d s)", minutes, seconds).toString();
        }
    }

    @Override
    public boolean search(String query) {
        return part.getPath().toLowerCase().contains(query);
    }

    public boolean isExpired() {
        return part.getTimeRemaining() - Calendar.getInstance().getTimeInMillis() < 0;
    }
}
