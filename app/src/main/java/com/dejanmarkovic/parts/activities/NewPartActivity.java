package com.dejanmarkovic.parts.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dejanmarkovic.parts.PartsApplication;
import com.dejanmarkovic.parts.R;
import com.dejanmarkovic.parts.presenters.NewPartPresenter;
import com.dejanmarkovic.parts.repository.entities.Part;
import com.dejanmarkovic.parts.util.SpinnerDialog;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.define.Define;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class NewPartActivity extends AppCompatActivity implements NewPartPresenter.View {

    private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy. (HH:mm)");
    private static final String PART = "PART";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.part_name)
    AutoCompleteTextView partName;
    @BindView(R.id.category)
    Spinner category;
    @BindView(R.id.model)
    Spinner model;
    @BindView(R.id.fuel_chooser)
    Spinner fuels;
    @BindView(R.id.year_manufactured)
    Spinner year;
    @BindView(R.id.chasis)
    Spinner chasis;
    @BindView(R.id.power)
    EditText power;
    @BindView(R.id.image_chooser)
    AppCompatButton imageChooser;
    @BindView(R.id.new_part)
    CheckBox newPart;
    @BindView(R.id.used_part)
    CheckBox usedPart;
    @BindView(R.id.time_picker)
    Spinner picker;

    @Inject
    NewPartPresenter presenter;

    private SpinnerDialog dialog;
    private ArrayList<String> categories;
    private Calendar valid = Calendar.getInstance();

    @OnItemSelected(R.id.category)
    public void chooseCategory() {
        String category = this.category.getSelectedItem().toString();
        presenter.loadModels(category);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_part);
        ButterKnife.bind(this);
        ((PartsApplication) getApplication()).getApplicationComponent().inject(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.new_request);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dialog = new SpinnerDialog(this);
        //updateProgress(true);
    }

    public static void launchOfferCreation(Context context) {
        context.startActivity(new Intent(context, NewPartActivity.class));
    }

    public static void editPartOffer(Context context, Part part) {
        Intent intent = new Intent(context, NewPartActivity.class);
        intent.putExtra(PART, part);
        context.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_part, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_send) {
            if (validForm()) {
                Part part = new Part();
                part.setCategory(category.getSelectedItem().toString());
                part.setModel(model.getSelectedItem().toString());
                part.setPartName(partName.getText().toString());
                part.setYear(Integer.parseInt(year.getSelectedItem().toString()));
                part.setTimeCreated(Calendar.getInstance().getTimeInMillis());
                part.setTimeToLive(calculateValidTime());
                part.setFuel(fuels.getSelectedItem().toString());
                part.setChasis(chasis.getSelectedItem().toString());
                part.setNewPart(newPart.isChecked());
                part.setUsedPart(usedPart.isChecked());

                presenter.setPart(part);
                presenter.submit(category.getSelectedItem().toString(),
                        model.getSelectedItem().toString(),
                        partName.getText().toString());
            }
            return true;
        }
        if (item.getItemId() == android.R.id.home) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.warning)
                    .setMessage(R.string.warning_discarding_new)
                    .setPositiveButton(R.string.yes, (dialog12, which) -> onBackPressed())
                    .setNegativeButton(R.string.no, null)
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private long calculateValidTime() {
        return Calendar.getInstance().getTimeInMillis() + getSelectedIntervalMillis();
    }

    private long getSelectedIntervalMillis() {
        switch (picker.getSelectedItemPosition()) {
            case 1:
                //30m
                return 1800000;
            case 2:
                //1h
                return 3600000;
            case 3:
                //12h
                return 43200000;
            case 4:
                //1d
                return 86400000;
            case 5:
                //7d
                return 604800000;
        }
        return 0;
    }


    private boolean validForm() {
        if (partName.getText().toString().isEmpty()) {
            partName.setError(getString(R.string.error_field_required));
            return false;
        }
        if (power.getText().toString().isEmpty()) {
            power.setError(getString(R.string.error_field_required));
            return false;
        }
        if (picker.getSelectedItem().toString().equals(getString(R.string.choose_time))) {
            Toast.makeText(this, R.string.please_choose_time, Toast.LENGTH_SHORT).show();
            return false;
        }
        try {
            Integer.parseInt(year.getSelectedItem().toString());
        } catch (Exception e) {
            Toast.makeText(this, R.string.please_choose_year, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (chasis.getSelectedItem().toString().equals(getString(R.string.chasis))) {
            Toast.makeText(this, R.string.please_choose_chasis, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (fuels.getSelectedItem().toString().equals(getString(R.string.choose_fuel))) {
            Toast.makeText(this, R.string.please_choose_fuel_type, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @OnClick(R.id.image_chooser)
    public void getImages() {
        FishBun.with(this)
                .setMaxCount(5)
                .setButtonInAlbumActivity(false)
                .setCamera(true)
                .setReachLimitAutomaticClose(false)
                .setAllViewTitle(getString(R.string.all))
                .setActionBarTitle(getString(R.string.choose_photos))
                .textOnImagesSelectionLimitReached(getString(R.string.limit_reached))
                .startAlbum();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageData) {
        super.onActivityResult(requestCode, resultCode, imageData);
        switch (requestCode) {
            case Define.ALBUM_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    ArrayList<Parcelable> images = imageData.getParcelableArrayListExtra(Define.INTENT_PATH);
                    imageChooser.setText(images.size() +
                            " " + getResources().getQuantityText(images.size(), R.plurals.picture));
                    presenter.setImages(images);
                    break;
                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onResume(this);
        }
        if (categories == null) {
            categories = presenter.loadCategories();
            category.setAdapter(new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_dropdown_item, categories));
        }
        Part partToLoad = getIntent().getParcelableExtra(PART);
        if (partToLoad != null) {
            populateForm(partToLoad);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.onPause();
        }
        presenter.cancel(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onDestroy();
        }
        if (dialog != null && dialog.isShowing()) dialog.dismiss();
        dialog = null;
    }

    @Override
    public void updateViews(Part part) {

    }

    @Override
    public void updateProgress(boolean showProgress) {
        if (dialog != null) {
            if (showProgress) dialog.show();
            else dialog.dismiss();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void updateModels(List<String> models) {
        model.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, models));
    }

    @Override
    public void openLoginForm() {
        LoginActivity.launchLoginOrRegistrationWithReturn(this, getClass());
    }

    public void populateForm(Part part) {
        category.setSelection(presenter.getCategoryPosition(part.getCategory()));
        //model.setSelection();
        partName.setText(part.getPartName());
        fuels.setSelection(Arrays.binarySearch(getResources()
                .getIntArray(R.array.years), part.getYear()));
        fuels.setSelection(Arrays.binarySearch(getResources()
                .getStringArray(R.array.fuel_type), part.getFuel()));
        chasis.setSelection(Arrays.binarySearch(getResources()
                .getStringArray(R.array.chasis_types), part.getChasis()));
        if (part.isNewPart()) newPart.setChecked(true);
        if (part.isUsedPart()) usedPart.setChecked(true);
    }

}
