package com.dejanmarkovic.parts.activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.dejanmarkovic.parts.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.find_part)
    public void findPart() {
        ContentLandingActivity.launchContentActivity(this, ContentLandingActivity.PARTS);
    }

    @OnClick(R.id.find_transport)
    public void findTransport() {
        ContentLandingActivity.launchContentActivity(this, ContentLandingActivity.TRANSPORT);
    }

    @OnClick(R.id.find_tow)
    public void findTow() {
        ContentLandingActivity.launchContentActivity(this, ContentLandingActivity.TOW);
    }

    @OnClick(R.id.register)
    public void registerOrLogin() {
        LoginActivity.launchLoginOrRegistration(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.app_name);

        if(!isGooglePlayServicesAvailable(this)){
            //todo
        }
    }

    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            if(googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }
}
