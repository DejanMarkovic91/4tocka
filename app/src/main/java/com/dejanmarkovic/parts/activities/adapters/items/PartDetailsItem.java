package com.dejanmarkovic.parts.activities.adapters.items;

import com.dejanmarkovic.parts.repository.entities.Part;

/**
 * Created by dejanmarkovic on 4/19/17.
 */

public class PartDetailsItem extends PartItem {

    private Part part;
    private int position;

    public PartDetailsItem(String path, Loader loader, Presenter presenter, int position) {
        super(DETAILS);
        this.position = position;
        loader.loadFromPath(position, presenter, path.replaceAll("@", "/"));
    }

    public PartDetailsItem(Part part) {
        super(DETAILS);
        this.part = part;
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    @Override
    public boolean search(String query) {
        if (part != null)
            return part.getCategory().toLowerCase().contains(query) ||
                    part.getModel().toLowerCase().contains(query) ||
                    part.getChasis().toLowerCase().contains(query) ||
                    part.getPartName().toLowerCase().contains(query) ||
                    part.getFuel().toLowerCase().contains(query) ||
                    String.valueOf(part.getYear()).contains(query);
        else return false;
    }

    public interface Loader {

        void loadFromPath(int position, Presenter presenter, String path);

    }

    public interface Presenter {

        void itemChanged(Part part, int position);

    }
}
