package com.dejanmarkovic.parts.activities.adapters.items;

/**
 * Created by dejanmarkovic on 4/19/17.
 */

public abstract class PartItem implements Searchable {

    public static final int HEADER = 1;
    public static final int DETAILS = 2;
    public static final int RENEW = 2;

    private int type;

    public PartItem(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

}
