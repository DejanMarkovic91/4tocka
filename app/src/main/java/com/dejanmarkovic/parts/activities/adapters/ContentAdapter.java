package com.dejanmarkovic.parts.activities.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.dejanmarkovic.parts.R;
import com.dejanmarkovic.parts.activities.adapters.holders.ContentViewHolder;
import com.dejanmarkovic.parts.activities.adapters.items.PartDetailsItem;
import com.dejanmarkovic.parts.activities.adapters.items.PartHeaderItem;
import com.dejanmarkovic.parts.activities.adapters.items.PartItem;
import com.dejanmarkovic.parts.repository.entities.Part;
import com.dejanmarkovic.parts.repository.entities.PartDescriptor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dejanmarkovic on 4/11/17.
 */

public class ContentAdapter extends RecyclerView.Adapter<ContentViewHolder>
        implements PartDetailsItem.Presenter, Filterable {

    private boolean expandable;
    private List<PartItem> originalItems = new ArrayList<>();
    private List<PartItem> items = new ArrayList<>();
    private PartDetailsItem.Loader loader;
    private PartFilter partFilter;
    private ContextMenuListener contextMenuListener;

    public ContentAdapter(boolean expandable) {
        this.expandable = expandable;
    }

    public void setPresenter(PartDetailsItem.Loader loader) {
        this.loader = loader;
    }

    public void setContextMenuListener(ContextMenuListener listener) {
        this.contextMenuListener = listener;
    }

    public void setItems(List<PartItem> items) {
        this.originalItems = items;
        this.items = items;
    }

    @Override
    public ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == PartItem.HEADER) {
            return new ContentViewHolder(
                    LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_offer_header, parent, false));
        } else {
            return new ContentViewHolder(
                    LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_offer_details, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(ContentViewHolder holder, int position) {
        if (getItemViewType(position) == PartItem.HEADER) {
            PartHeaderItem item = (PartHeaderItem) items.get(position);
            holder.itemView.setOnClickListener(view -> expandOrCollapseItem(position));
            holder.itemView.setOnLongClickListener(view -> {
                if (contextMenuListener != null) {
                    contextMenuListener.openPartEditing(item.getPart());
                }
                return false;
            });
            String[] data = item.getPart().getPath().split("@");
            //holder.title.setText(String.format("%s %s", data[3], item.getFormattedTime()));
            if (!item.isExpired()) {
                Observable.interval(500, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aLong ->
                                holder.title.setText(String.format("%s %s",
                                        data[3], item.getFormattedTime())));
            } else {
                holder.title.setText(String.format("%s %s",
                        data[3], item.getFormattedTime()));
            }
            holder.description.setText(String.format("%s(%s)", data[1], data[2]));
        } else if (getItemViewType(position) == PartItem.DETAILS) {
            PartDetailsItem item = (PartDetailsItem) items.get(position);
            if (item.getPart() != null) {
                Context context = holder.itemView.getContext();
                StringBuilder builder = new StringBuilder();
                builder.append(context.getString(R.string.chasis))
                        .append(": ")
                        .append(item.getPart().getChasis())
                        .append("\n")
                        .append(context.getString(R.string.year_manufactured))
                        .append(": ")
                        .append(item.getPart().getYear())
                        .append("\n").append(context.getString(R.string.choose_fuel))
                        .append(": ")
                        .append(item.getPart().getFuel())
                        .append("\n");
                holder.descriptionText.setText(builder.toString());
                holder.accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
                holder.decline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            }
        }
    }

    private void expandOrCollapseItem(int position) {
        if (position == RecyclerView.NO_POSITION
                || getItemViewType(position) != PartItem.HEADER || !expandable) {
            return;
        }
        PartHeaderItem item = (PartHeaderItem) items.get(position);
        if (!item.isExpanded()) {
            item.setExpanded(true);
            items.add(position + 1, new PartDetailsItem(item.getPart().getPath(), loader, this, position + 1));
            notifyItemRangeChanged(position + 1, position + 2);
        } else {
            item.setExpanded(false);
            items.remove(position + 1);
            notifyItemRemoved(position + 1);
            notifyItemRangeChanged(position, getItemCount() - position);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public void itemChanged(Part part, int position) {
        ((PartDetailsItem) items.get(position)).setPart(part);
        notifyItemChanged(position);
    }

    public void setExpandable(boolean expandable) {
        this.expandable = expandable;
    }

    @Override
    public Filter getFilter() {
        if (partFilter == null) {
            partFilter = new PartFilter();
        }

        return partFilter;
    }

    private class PartFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<PartItem> tempList = new ArrayList<>();

                // search content in friend list
                for (PartItem item : originalItems) {
                    if (item.search(constraint.toString().toLowerCase())) {
                        tempList.add(item);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = originalItems.size();
                filterResults.values = originalItems;
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         *
         * @param constraint text
         * @param results    filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList<PartItem>) results.values;
            notifyDataSetChanged();
        }
    }

    public interface ContextMenuListener {

        void openPartEditing(PartDescriptor item);
    }
}
