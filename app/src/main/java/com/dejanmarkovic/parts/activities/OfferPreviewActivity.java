package com.dejanmarkovic.parts.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.dejanmarkovic.parts.PartsApplication;
import com.dejanmarkovic.parts.R;
import com.dejanmarkovic.parts.presenters.OfferPreviewPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dejan on 29-Apr-17.
 */

public class OfferPreviewActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    OfferPreviewPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_preview);
        ButterKnife.bind(this);
        ((PartsApplication) getApplication()).getApplicationComponent().inject(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.new_request);
    }
}
