package com.dejanmarkovic.parts.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.dejanmarkovic.parts.PartsApplication;
import com.dejanmarkovic.parts.R;
import com.dejanmarkovic.parts.activities.adapters.ContentAdapter;
import com.dejanmarkovic.parts.activities.adapters.items.PartDetailsItem;
import com.dejanmarkovic.parts.activities.adapters.items.PartItem;
import com.dejanmarkovic.parts.presenters.ContentLandingPresenter;
import com.dejanmarkovic.parts.repository.entities.PartDescriptor;
import com.dejanmarkovic.parts.util.SpinnerDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContentLandingActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ContentLandingPresenter.View,
        PartDetailsItem.Loader,
        SearchView.OnQueryTextListener,
        ContentAdapter.ContextMenuListener {

    private static final String CONTENT_TYPE = "CONTENT_TYPE";

    public static final int NONE = 0;
    public static final int PARTS = 1;
    public static final int TRANSPORT = 2;
    public static final int TOW = 3;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.content_list)
    RecyclerView contentList;

    @Inject
    ContentLandingPresenter presenter;

    private ContentAdapter adapter;
    private int contentType = NONE;
    private SpinnerDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_landing);
        ((PartsApplication) getApplication()).getApplicationComponent().inject(this);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.offers);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        LinearLayoutManager layoutManager =
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        contentList.setLayoutManager(layoutManager);
        DividerItemDecoration dividerDecoration = new DividerItemDecoration(this,
                layoutManager.getOrientation());
        dividerDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));
        contentList.addItemDecoration(dividerDecoration);
        contentList.setAdapter(adapter = new ContentAdapter(false));
        adapter.setContextMenuListener(this);

        dialog = new SpinnerDialog(this);
    }

    @Override
    public void openPartEditing(PartDescriptor item) {
        //updateProgress(true);
        presenter.loadPartDetails(item.getPath())
                .subscribe(
                        part -> {
                            //updateProgress(false);
                            NewPartActivity.editPartOffer(ContentLandingActivity.this, part);
                        },
                        throwable -> {
                            //updateProgress(false);
                            throwable.printStackTrace();
                        }
                );
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_login) {
            LoginActivity.launchLoginOrRegistration(this);
        } else if (id == R.id.nav_logout) {
            presenter.logout();
            finish();
        } else if (id == R.id.nav_settings) {
            NewPartActivity.launchOfferCreation(this);
        } else if (id == R.id.nav_find_part) {
            ContentLandingActivity.launchContentActivity(this, ContentLandingActivity.PARTS);
        } else if (id == R.id.nav_find_transport) {
            ContentLandingActivity.launchContentActivity(this, ContentLandingActivity.TRANSPORT);
        } else if (id == R.id.nav_find_tow) {
            ContentLandingActivity.launchContentActivity(this, ContentLandingActivity.TOW);
        } else if (id == R.id.nav_login) {
            LoginActivity.launchLoginOrRegistration(this);
        } else if (id == R.id.nav_chat) {
            ChatActivity.openChat(this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_content, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            NewPartActivity.launchOfferCreation(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void showContent(Context context) {
        context.startActivity(new Intent(context, ContentLandingActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onDestroy();
        }
    }

    public static void launchContentActivity(Context context, int contentType) {
        Intent intent = new Intent(context, ContentLandingActivity.class);
        intent.putExtra(CONTENT_TYPE, contentType);
        context.startActivity(intent);
    }

    @Override
    public void setCarCategories(ArrayList<String> categories) {

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void setParts(List<PartItem> parts) {
        adapter.setItems(parts);
        adapter.setPresenter(this);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateAdapter(boolean expandable) {
        adapter.setExpandable(expandable);
    }

    @Override
    public void loadFromPath(int position, PartDetailsItem.Presenter p, String path) {
        new AlertDialog.Builder(this)
                .setAdapter(new ArrayAdapter<>(this,
                                android.R.layout.activity_list_item,
                                new String[]{getString(R.string.renew_offer)}),
                        (dialog1, which) -> {
                            if (which == 0) {
                                presenter.loadPartDetails(path)
                                        .subscribe(part -> p.itemChanged(part, position),
                                                throwable -> throwable.printStackTrace());
                            }
                        }).setCancelable(true)
                .show();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }

    public void updateProgress(boolean showProgress) {
        if (dialog != null) {
            if (showProgress) dialog.show();
            else dialog.dismiss();
        }
    }
}
