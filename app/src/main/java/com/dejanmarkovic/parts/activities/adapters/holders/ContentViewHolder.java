package com.dejanmarkovic.parts.activities.adapters.holders;

import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dejanmarkovic.parts.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dejanmarkovic on 4/11/17.
 */

public class ContentViewHolder extends RecyclerView.ViewHolder
        implements View.OnCreateContextMenuListener {

    @Nullable
    @BindView(R.id.image_preview)
    public ImageView imagePreview;
    @Nullable
    @BindView(R.id.title)
    public TextView title;
    @Nullable
    @BindView(R.id.description_title)
    public TextView description;
    @Nullable
    @BindView(R.id.description_text)
    public TextView descriptionText;
    @Nullable
    @BindView(R.id.accept)
    public AppCompatButton accept;
    @Nullable
    @BindView(R.id.decline)
    public AppCompatButton decline;

    public ContentViewHolder(View itemView) {
        super(itemView);
        itemView.setOnCreateContextMenuListener(this);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle(R.string.part_item_context_menu_header);
        menu.add(0, v.getId(), 0, R.string.renew_offer);
    }
}
