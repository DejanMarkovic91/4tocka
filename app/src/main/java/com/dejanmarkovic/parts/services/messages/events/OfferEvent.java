package com.dejanmarkovic.parts.services.messages.events;

import android.content.Intent;

import com.dejanmarkovic.parts.activities.OfferPreviewActivity;
import com.dejanmarkovic.parts.services.messages.ReceivedMessage;

/**
 * Created by dejanmarkovic on 4/26/17.
 */

public class OfferEvent extends Event {
    @Override
    public void processData(ReceivedMessage receivedMessage, int type) {
        Intent intent = new Intent(receivedMessage.getContext(), OfferPreviewActivity.class);
        presentSimpleNotification(receivedMessage.getContext(),
                "Potreban deo",
                receivedMessage.get(ReceivedMessage.PART_ID),
                intent);
    }
}
