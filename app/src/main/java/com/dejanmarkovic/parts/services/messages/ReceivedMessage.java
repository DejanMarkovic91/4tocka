package com.dejanmarkovic.parts.services.messages;

import android.content.Context;
import android.os.Bundle;

import com.dejanmarkovic.parts.services.messages.events.DoNothingEvent;
import com.dejanmarkovic.parts.services.messages.events.Event;
import com.dejanmarkovic.parts.services.messages.events.GeneralNotificationEvent;
import com.dejanmarkovic.parts.services.messages.events.OfferEvent;

import java.util.Map;

/**
 * Created by dejanmarkovic on 4/26/17.
 */

public class ReceivedMessage {

    private static final int GENERAL_NOTIFICATION = 1;
    public static final int OFFER_MESSAGE = 2;

    public static final String TYPE = "TYPE";
    public static final String PART_ID = "PART_ID";
    private Event notificationEvent;

    private int type;
    private Map<String, String> data;
    private Context context;

    public ReceivedMessage(Context context, Map<String, String> data) {
        this.data = data;
        this.context = context;
        if (data.get(TYPE) != null) {
            type = Integer.parseInt(data.get(TYPE));
            notificationEvent = parseNotificationType();
            notificationEvent.processData(this, type);
        }
    }

    private Event parseNotificationType() {
        switch (type) {
            case GENERAL_NOTIFICATION:
                return new GeneralNotificationEvent();
            case OFFER_MESSAGE:
                return new OfferEvent();
            default:
                return new DoNothingEvent();
        }
    }

    public int getType() {
        return type;
    }

    public Context getContext() {
        return context;
    }

    public String get(String key) {
        return data.get(key);
    }
}
