package com.dejanmarkovic.parts.services;

import com.dejanmarkovic.parts.services.messages.ReceivedMessage;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by dejanmarkovic on 4/12/17.
 */

public class FCMMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        new ReceivedMessage(this, remoteMessage.getData());
    }
}
