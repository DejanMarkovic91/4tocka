package com.dejanmarkovic.parts.services.messages.events;

import android.util.Log;

import com.dejanmarkovic.parts.services.messages.ReceivedMessage;

/**
 * Created by dejanmarkovic on 4/26/17.
 */

public class DoNothingEvent extends Event {
    @Override
    public void processData(ReceivedMessage receivedMessage, int type) {
        Log.e("ERROR", "Unknown receivedMessage type: " + type);
    }
}
