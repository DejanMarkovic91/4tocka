package com.dejanmarkovic.parts;

import android.app.Application;
import com.squareup.leakcanary.LeakCanary;


public final class PartsBuildProcedure {
    private PartsBuildProcedure() {

    }

    public static boolean isAnalyzerProcess(Application application) {
        return LeakCanary.isInAnalyzerProcess(application);
    }

    public static void buildDebugTools(Application application) {
        LeakCanary.install(application);
        StethoUtils.install(application);
        //Fabric.with(application, new Crashlytics());
    }
}
