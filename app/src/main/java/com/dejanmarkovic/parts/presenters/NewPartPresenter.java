package com.dejanmarkovic.parts.presenters;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;

import com.cocosw.favor.FavorAdapter;
import com.dejanmarkovic.parts.features.FirebaseDatabaseFeature;
import com.dejanmarkovic.parts.features.LoginFeature;
import com.dejanmarkovic.parts.repository.entities.Models;
import com.dejanmarkovic.parts.repository.entities.Part;
import com.dejanmarkovic.parts.util.Config;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by dejanmarkovic on 4/11/17.
 */

public class NewPartPresenter implements Presenter<NewPartPresenter.View> {

    private View parentView;
    private LoginFeature loginFeature;
    private FirebaseDatabaseFeature databaseFeature;
    private ArrayList<Parcelable> images = new ArrayList<>();
    private Part part = new Part();
    private Config prefsAdapter;
    private ArrayList<String> categories = new ArrayList<>();

    public NewPartPresenter(LoginFeature loginFeature, FirebaseDatabaseFeature databaseFeature) {
        this.loginFeature = loginFeature;
        this.databaseFeature = databaseFeature;
    }

    @Override
    public void onResume(View parentView) {
        this.parentView = parentView;
        this.prefsAdapter = new FavorAdapter.Builder(parentView.getContext())
                .build().create(Config.class);
    }

    @Override
    public void onPause() {
        this.parentView = null;
    }

    @Override
    public void onDestroy() {
        this.parentView = null;
    }

    public void setImages(ArrayList<Parcelable> images) {
        this.images = images;
    }

    public void cancel(boolean shouldUpdateView) {
        images.clear();
        part = new Part();
        if (shouldUpdateView)
            parentView.updateViews(part);
    }

    public void submit(String category, String model, String part) {
        if (loginFeature.getCurrentUser() != null) {
            if (databaseFeature.savePart(parentView.getContext(), category,
                    model, part, images, this.part))
                ((Activity) parentView.getContext()).finish();
        } else {
            //please authenticate
            parentView.openLoginForm();
        }
    }

    public ArrayList<String> loadCategories() {
        if (prefsAdapter.getCategories() != null) {
            categories = new ArrayList();
            for (Models m : prefsAdapter.getCategories())
                categories.add(m.getName());
        }
        if (categories == null) return new ArrayList<>();
        return categories;
    }

    public void loadModels(String category) {
        if (prefsAdapter.getCategories() != null) {
            List<Models> categories = prefsAdapter.getCategories();
            for (Models c : categories) {
                if (c.getName().equals(category)) {
                    parentView.updateModels(c.getModels());
                    return;
                }
            }
        }

        parentView.updateProgress(false);
        parentView.updateModels(new ArrayList<>());
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public int getCategoryPosition(String category) {
        return prefsAdapter.getCategories().indexOf(category);
    }

    public interface View {

        void updateViews(Part part);

        void updateProgress(boolean showProgress);

        Context getContext();

        void updateModels(List<String> models);

        void openLoginForm();
    }
}
