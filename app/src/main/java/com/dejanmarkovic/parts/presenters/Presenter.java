package com.dejanmarkovic.parts.presenters;

/**
 * Created by dejanmarkovic on 4/11/17.
 */

public interface Presenter<T> {

    void onResume(T parentView);

    void onPause();

    void onDestroy();
}
