package com.dejanmarkovic.parts.presenters;


import com.dejanmarkovic.parts.features.FirebaseDatabaseFeature;
import com.dejanmarkovic.parts.features.LoginFeature;

/**
 * Created by dejan on 29-Apr-17.
 */

public class OfferPreviewPresenter implements Presenter<OfferPreviewPresenter.View> {


    private LoginFeature loginFeature;
    private FirebaseDatabaseFeature databaseFeature;
    private View parentView;

    public OfferPreviewPresenter(LoginFeature loginFeature,
                                 FirebaseDatabaseFeature databaseFeature) {
        this.loginFeature = loginFeature;
        this.databaseFeature = databaseFeature;
    }

    @Override
    public void onResume(View parentView) {
        this.parentView = parentView;
    }

    @Override
    public void onPause() {
        this.parentView = null;
    }

    @Override
    public void onDestroy() {
        this.parentView = null;
    }

    public interface View {

    }

}
