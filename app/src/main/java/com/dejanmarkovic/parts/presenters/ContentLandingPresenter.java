package com.dejanmarkovic.parts.presenters;

import android.content.Context;
import android.widget.Toast;

import com.cocosw.favor.FavorAdapter;
import com.dejanmarkovic.parts.R;
import com.dejanmarkovic.parts.activities.adapters.items.PartItem;
import com.dejanmarkovic.parts.features.FirebaseDatabaseFeature;
import com.dejanmarkovic.parts.features.LoginFeature;
import com.dejanmarkovic.parts.repository.entities.Models;
import com.dejanmarkovic.parts.repository.entities.Part;
import com.dejanmarkovic.parts.repository.entities.UserDetails;
import com.dejanmarkovic.parts.util.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dejanmarkovic on 4/11/17.
 */

public class ContentLandingPresenter implements Presenter<ContentLandingPresenter.View> {

    private Config prefsAdapter;
    private ArrayList categories = new ArrayList();
    private View parentView;
    private LoginFeature feature;
    private FirebaseDatabaseFeature databaseFeature;
    private UserDetails details;

    public ContentLandingPresenter(LoginFeature feature, FirebaseDatabaseFeature databaseFeature) {
        this.feature = feature;
        this.databaseFeature = databaseFeature;
    }

    @Override
    public void onResume(View parentView) {
        this.parentView = parentView;
        this.prefsAdapter = new FavorAdapter.Builder(parentView.getContext())
                .build().create(Config.class);
        if (prefsAdapter.getCategories() == null)
            databaseFeature.getCarCategoriesObservable()
                    .subscribe(models -> {
                        categories = new ArrayList();
                        for (Models m : models)
                            categories.add(m.getName());
                        prefsAdapter.setCategories(new ArrayList<>(models));
                        parentView.setCarCategories(categories);
                    }, throwable -> throwable.printStackTrace());

        //load parts that i offered to buy

        if (feature.getCurrentUser() != null) {
            databaseFeature.getMyPartsRequests()
                    .subscribe(parts -> parentView.setParts(parts),
                            throwable -> throwable.printStackTrace());
            feature.getUserDetails().subscribe(
                    userDetails -> parentView.updateAdapter(userDetails.getCompany() != null),
                    throwable -> throwable.printStackTrace());
        } else {
            Toast.makeText(parentView.getContext(), R.string.please_login, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        this.parentView = null;
    }

    @Override
    public void onDestroy() {
        this.parentView = null;
    }

    public void logout() {
        feature.logout();
    }

    public Observable<Part> loadPartDetails(String path) {
        return databaseFeature.loadPartDetails(path);
    }

    public interface View {

        void setCarCategories(ArrayList<String> categories);

        Context getContext();

        void setParts(List<PartItem> parts);

        void updateAdapter(boolean expandable);
    }
}
