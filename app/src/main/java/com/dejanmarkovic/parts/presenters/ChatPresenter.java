package com.dejanmarkovic.parts.presenters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.View;

import com.dejanmarkovic.parts.R;
import com.dejanmarkovic.parts.features.FirebaseDatabaseFeature;
import com.dejanmarkovic.parts.features.LoginFeature;
import com.github.bassaer.chatmessageview.models.Message;
import com.github.bassaer.chatmessageview.models.User;
import com.github.bassaer.chatmessageview.utils.ChatBot;
import com.github.bassaer.chatmessageview.utils.IMessageStatusIconFormatter;
import com.github.bassaer.chatmessageview.utils.IMessageStatusTextFormatter;
import com.github.bassaer.chatmessageview.views.ChatView;

import java.util.Random;

/**
 * Created by dejanm on 05-May-17.
 */

public class ChatPresenter implements Presenter<ChatPresenter.View> {

    View parentView;
    private User[] users = new User[2];
    private LoginFeature loginFeature;
    private FirebaseDatabaseFeature feature;

    public ChatPresenter(LoginFeature loginFeature, FirebaseDatabaseFeature feature) {
        this.loginFeature = loginFeature;
        this.feature = feature;
    }

    @Override
    public void onResume(View parentView) {
        this.parentView = parentView;
    }

    @Override
    public void onPause() {
        parentView = null;
    }

    @Override
    public void onDestroy() {
        parentView = null;
    }

    public void initChat(ChatView chatView) {

        users[0] = new User(loginFeature.getCurrentUser().getUid().hashCode(),
                loginFeature.getCurrentUser().getDisplayName(),
                BitmapFactory.decodeResource(parentView.getContext().getResources(),
                        R.drawable.ic_face));
        users[1] = new User(0,
                "Partner",
                BitmapFactory.decodeResource(parentView.getContext().getResources(),
                        R.drawable.ic_face));
        chatView.setRightBubbleColor(
                ContextCompat.getColor(parentView.getContext(), R.color.light_blue));
        chatView.setLeftBubbleColor(
                ContextCompat.getColor(parentView.getContext(), R.color.indigo));
        chatView.setBackgroundColor(Color.WHITE);
        chatView.setUsernameTextColor(Color.GRAY);
        chatView.setSendTimeTextColor(Color.GRAY);
        chatView.setDateSeparatorColor(Color.GRAY);
        chatView.setInputTextHint(parentView.getContext().getString(R.string.hint_enter_a_message));

        chatView.setOnClickSendButtonListener(view -> {
            //new message
            Message message = new Message.Builder()
                    .setUser(users[0])
                    .setRightMessage(true)
                    .setMessageText(chatView.getInputText())
                    .hideIcon(true)
                    .build();
            //Set to chat view
            chatView.send(message);
            //Reset edit text
            chatView.setInputText("");

            //Receive message
            final Message receivedMessage = new Message.Builder()
                    .setUser(users[1])
                    .setRightMessage(false)
                    .setMessageText(ChatBot.talk(users[0].getName(), message.getMessageText()))
                    .build();

            // This is a demo bot
            // Return within 3 seconds
            int sendDelay = (new Random().nextInt(7) + 1) * 1000;
            new Handler().postDelayed(() -> chatView.receive(receivedMessage), sendDelay);
        });

        chatView.setOnClickOptionButtonListener(view -> {
            Intent intent;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                intent = new Intent(Intent.ACTION_GET_CONTENT);
            } else {
                intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }
            intent.setType("image/*");

            parentView.requestImages(intent);
        });

    }

    public void addImageToChat(ChatView chatView, Bitmap picture) {
        Message message = new Message.Builder()
                .setRightMessage(true)
                .setMessageText(Message.Type.PICTURE.name())
                .setUser(users[0])
                .hideIcon(true)
                .setPicture(picture)
                .setType(Message.Type.PICTURE)
                .setStatusIconFormatter(new MessageStatusFormatter(parentView.getContext()))
                .setMessageStatusType(Message.MESSAGE_STATUS_ICON)
                .setStatus(MessageStatusFormatter.STATUS_DELIVERED)
                .build();
        chatView.send(message);
    }

    public interface View {

        Activity getContext();

        void requestImages(Intent intent);
    }

    public class MessageStatusFormatter implements IMessageStatusIconFormatter,
            IMessageStatusTextFormatter {
        public static final int STATUS_DELIVERING = 0;
        public static final int STATUS_DELIVERED = 1;
        public static final int STATUS_SEEN = 2;
        public static final int STATUS_ERROR = 3;
        private Drawable mDeliveringIcon;
        private Drawable mDeliveredIcon;
        private Drawable mSeenIcon;
        private Drawable mErrorIcon;
        private String mDeliveringText;
        private String mDeliveredText;
        private String mSeenText;
        private String mErrorText;

        public MessageStatusFormatter(Context context) {
            //Init icons
            mDeliveringIcon = DrawableCompat.wrap(
                    ContextCompat.getDrawable(context, R.drawable.ic_mail_outline));
            mDeliveredIcon = DrawableCompat.wrap(
                    ContextCompat.getDrawable(context, R.drawable.ic_done));
            mSeenIcon = DrawableCompat.wrap(
                    ContextCompat.getDrawable(context, R.drawable.ic_done_all));
            mErrorIcon = DrawableCompat.wrap(
                    ContextCompat.getDrawable(context, R.drawable.ic_report));
            //Set colors
            ColorStateList colorStateList = ColorStateList.valueOf(
                    ContextCompat.getColor(context, R.color.blueGray500));
            DrawableCompat.setTintList(mErrorIcon, colorStateList);
            DrawableCompat.setTintList(mDeliveringIcon, colorStateList);
            DrawableCompat.setTintList(mDeliveredIcon, colorStateList);
            DrawableCompat.setTintList(mSeenIcon, colorStateList);

            //Init status labels
            mDeliveringText = context.getString(R.string.sending);
            mDeliveredText = context.getString(R.string.sent);
            mSeenText = context.getString(R.string.seen);
            mErrorText = context.getString(R.string.error);
        }

        @Override
        public Drawable getStatusIcon(int status, boolean isRightMessage) {
            if (!isRightMessage) {
                return null;
            }
            switch (status) {
                case STATUS_DELIVERING:
                    return mDeliveringIcon;
                case STATUS_DELIVERED:
                    return mDeliveredIcon;
                case STATUS_SEEN:
                    return mSeenIcon;
                case STATUS_ERROR:
                    return mErrorIcon;
            }
            return null;
        }

        @Override
        public String getStatusText(int status, boolean isRightMessage) {
            switch (status) {
                case STATUS_DELIVERING:
                    return mDeliveringText;
                case STATUS_DELIVERED:
                    return mDeliveredText;
                case STATUS_SEEN:
                    return mSeenText;
                case STATUS_ERROR:
                    return mErrorText;
            }
            return "";
        }
    }
}
