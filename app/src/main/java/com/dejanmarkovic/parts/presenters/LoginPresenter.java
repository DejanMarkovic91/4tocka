package com.dejanmarkovic.parts.presenters;

import android.content.Context;
import android.text.TextUtils;

import com.cocosw.favor.FavorAdapter;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SignUpEvent;
import com.dejanmarkovic.parts.activities.LoginActivity;
import com.dejanmarkovic.parts.features.FirebaseDatabaseFeature;
import com.dejanmarkovic.parts.features.FirebaseMessagingFeature;
import com.dejanmarkovic.parts.features.LoginFeature;
import com.dejanmarkovic.parts.repository.DatabaseRepository;
import com.dejanmarkovic.parts.repository.entities.Company;
import com.dejanmarkovic.parts.repository.entities.Models;
import com.dejanmarkovic.parts.util.Config;
import com.google.firebase.auth.EmailAuthProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dejanmarkovic on 4/11/17.
 */

public class LoginPresenter implements Presenter<LoginPresenter.View> {

    private View parentView;
    private LoginFeature loginFeature;
    private FirebaseDatabaseFeature firDatabaseFeature;
    private FirebaseMessagingFeature firMessagingFeature;
    private List<Models> models = new ArrayList<>();

    public LoginPresenter(LoginFeature loginFeature,
                          FirebaseDatabaseFeature firDatabaseFeature,
                          FirebaseMessagingFeature firMessagingFeature) {
        this.loginFeature = loginFeature;
        this.firDatabaseFeature = firDatabaseFeature;
        this.firMessagingFeature = firMessagingFeature;
    }

    public void attemptLogin(String email, String password) {

        //check for a valid password
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            parentView.showPasswordInvalidInfo();
            return;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email) || !isEmailValid(email)) {
            parentView.showEmailInvalidInfo();
            return;
        }

        parentView.showProgress(true);
        loginFeature.authenticate(EmailAuthProvider.getCredential(email, password))
                .subscribe(authResult ->
                        {
                            Crashlytics.setUserEmail(email);
                            Crashlytics.setUserIdentifier(loginFeature.getCurrentUser().getUid());
                            parentView.showProgress(false);
                            parentView.loginSuccess();
                        },
                        throwable -> {
                            parentView.showProgress(false);
                            parentView.showMessage(throwable.getLocalizedMessage());
                            throwable.printStackTrace();
                        });
    }

    public void attemptRegistration(String name,
                                    boolean company,
                                    String city,
                                    String cityCode,
                                    String address,
                                    String phone,
                                    String email,
                                    String password) {


        //check for a valid password
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            parentView.showPasswordInvalidInfo();
            return;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email) || !isEmailValid(email)) {
            parentView.showEmailInvalidInfo();
            return;
        }

        //check for a valid name
        if (TextUtils.isEmpty(name)) {
            parentView.showNameEmptyError();
            return;
        }

        //check for a valid city
        if (TextUtils.isEmpty(city)) {
            parentView.showCityEmptyError();
            return;
        }

        //check for a valid name
        if (TextUtils.isEmpty(cityCode)) {
            parentView.showCityCodeEmptyError();
            return;
        }

        //check for a valid address
        if (TextUtils.isEmpty(address)) {
            parentView.showAddressEmptyError();
            return;
        }

        //check for a valid name
        if (TextUtils.isEmpty(phone)) {
            parentView.showPhoneEmptyError();
            return;
        }

        parentView.showProgress(true);
        SignUpEvent signUpEvent = new SignUpEvent();
        signUpEvent.putMethod("email");
        signUpEvent.putSuccess(false);

        loginFeature.registerUser(email, password)
                .subscribe(authResult ->
                        {
                            parentView.showProgress(false);
                            signUpEvent.putSuccess(true);
                            Answers.getInstance().logSignUp(signUpEvent);
                            if (company) {
                                parentView.showModelChooserDialog(models1 -> {
                                    loginFeature.populateUserAdditionalData(name,
                                            new Company(models1), city, cityCode, address, phone);
                                    parentView.registrationSuccess();
                                    subscribeForModels(models1);
                                });
                            } else {
                                loginFeature.populateUserAdditionalData(name,
                                        null, city, cityCode, address, phone);
                                parentView.registrationSuccess();
                            }
                        },
                        throwable -> {
                            Answers.getInstance().logSignUp(signUpEvent);
                            parentView.showProgress(false);
                            parentView.showMessage(throwable.getLocalizedMessage());
                            throwable.printStackTrace();
                        });
    }

    private void subscribeForModels(List<Models> models) {
        List<String> topics = new ArrayList<>();
        for (Models m : models) {
            for (String type : m.getModels())
                topics.add(m.getName() + "@" + type);
        }
        firMessagingFeature.subscribeListOfTopics(topics);
    }

    private boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4 && password.matches(".*\\d+.*");
    }

    @Override
    public void onResume(View parentView) {
        this.parentView = parentView;

        Config prefsAdapter = new FavorAdapter.Builder(parentView.getContext())
                .build().create(Config.class);
        if (prefsAdapter.getCategories() == null) {
            parentView.showProgress(true);
            firDatabaseFeature.getCarCategoriesObservable()
                    .subscribe(models -> {
                        this.models = models;
                        parentView.showProgress(false);
                    }, throwable -> {
                        throwable.printStackTrace();
                        parentView.showProgress(false);
                    });
        } else {
            this.models = prefsAdapter.getCategories();
        }
    }

    public List<Models> getModels() {
        return models;
    }

    @Override
    public void onPause() {
        this.parentView = null;
    }

    @Override
    public void onDestroy() {
        this.parentView = null;
    }

    public interface View {

        void showPasswordInvalidInfo();

        void showEmailInvalidInfo();

        void loginSuccess();

        void showMessage(String localizedMessage);

        void showProgress(boolean show);

        void showNameEmptyError();

        void showCityEmptyError();

        void showCityCodeEmptyError();

        void showAddressEmptyError();

        void showPhoneEmptyError();

        void registrationSuccess();

        Context getContext();

        void showModelChooserDialog(LoginActivity.ModelSelectorCallback callback);
    }
}
