package com.dejanmarkovic.parts.repository.entities;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by dejanm on 27-Apr-17.
 */

public abstract class Entity implements Cloneable {

    protected static final String ID = "_id";

    public abstract long getId();

    public abstract String getTableName();

    public abstract ContentValues populateContentValues();

    public abstract void convertFromCursor(Cursor cursor);

    @Override
    public abstract Object clone();

}
