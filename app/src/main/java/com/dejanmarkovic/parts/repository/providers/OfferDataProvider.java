package com.dejanmarkovic.parts.repository.providers;

import com.dejanmarkovic.parts.repository.Criteria;
import com.dejanmarkovic.parts.repository.entities.Offer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by dejanmarkovic on 4/10/17.
 */
@Singleton
public class OfferDataProvider {

    //private final DefaultSourceStrategy sourceStrategy;

    @Inject
    public OfferDataProvider(/*DefaultSourceStrategy defaultSourceStrategy*/) {
        /*this.sourceStrategy = defaultSourceStrategy;
        this.externalApiWrapper = externalApiWrapper;
        this.matchSqlProvider = matchSqlProvider;*/
    }

    public List<Offer> getOffers(Criteria criteria) {
        return new ArrayList<>();
    }

    public void publishOffer(Offer offer) {

    }
}
