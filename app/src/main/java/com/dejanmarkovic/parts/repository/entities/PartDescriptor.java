package com.dejanmarkovic.parts.repository.entities;

/**
 * Created by dejanmarkovic on 4/19/17.
 */

public class PartDescriptor {
    private String path;
    private long timeCreated;
    private long timeRemaining;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }


    public void setTimeRemaining(long timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public long getTimeRemaining() {
        return timeRemaining;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PartDescriptor that = (PartDescriptor) o;

        if (timeCreated != that.timeCreated) return false;
        if (timeRemaining != that.timeRemaining) return false;
        return path != null ? path.equals(that.path) : that.path == null;

    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (int) (timeCreated ^ (timeCreated >>> 32));
        result = 31 * result + (int) (timeRemaining ^ (timeRemaining >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "PartDescriptor{" +
                "path='" + path + '\'' +
                ", timeCreated=" + timeCreated +
                ", timeRemaining=" + timeRemaining +
                '}';
    }
}
