package com.dejanmarkovic.parts.repository;

/**
 * Created by dejanm on 27-Apr-17.
 */

public class SimpleQuery {

    private String name;
    private long time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    //this method should be overridden
    public String getFormattedQuery() {
        return String.format("time = %d AND name = %s", time, name);
    }

    @Override
    public String toString() {
        return "SimpleQuery{" +
                "name='" + name + '\'' +
                ", time=" + time +
                '}';
    }
}
