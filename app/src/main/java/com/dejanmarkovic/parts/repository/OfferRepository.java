package com.dejanmarkovic.parts.repository;

import com.dejanmarkovic.parts.repository.entities.Offer;

import java.util.List;

/**
 * Created by dejanmarkovic on 4/10/17.
 */

public interface OfferRepository {

    List<Offer> getOffers(Criteria criteria);

    void publishOffer(Offer offer);
}
