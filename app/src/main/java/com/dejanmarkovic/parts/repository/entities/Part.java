package com.dejanmarkovic.parts.repository.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dejanmarkovic on 4/19/17.
 */

public class Part implements Parcelable{
    private String id;
    private String model;
    private String category;
    private String partName;
    private String chasis;
    private double power;
    private String fuel;
    private int year;
    private String userID;
    private long timeCreated;
    private long timeToLive;
    private String status;
    private String path;
    private boolean newPart;
    private boolean usedPart;
    private List<Image> images = new ArrayList<>();

    public Part(){}

    protected Part(Parcel in) {
        id = in.readString();
        model = in.readString();
        category = in.readString();
        partName = in.readString();
        chasis = in.readString();
        power = in.readDouble();
        fuel = in.readString();
        year = in.readInt();
        userID = in.readString();
        timeCreated = in.readLong();
        timeToLive = in.readLong();
        status = in.readString();
        path = in.readString();
        newPart = in.readByte() != 0;
        usedPart = in.readByte() != 0;
    }

    public static final Creator<Part> CREATOR = new Creator<Part>() {
        @Override
        public Part createFromParcel(Parcel in) {
            return new Part(in);
        }

        @Override
        public Part[] newArray(int size) {
            return new Part[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    public long getTimeToLive() {
        return timeToLive;
    }

    public void setTimeToLive(long timeToLive) {
        this.timeToLive = timeToLive;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isNewPart() {
        return newPart;
    }

    public void setNewPart(boolean newPart) {
        this.newPart = newPart;
    }

    public boolean isUsedPart() {
        return usedPart;
    }

    public void setUsedPart(boolean usedPart) {
        this.usedPart = usedPart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Part part = (Part) o;

        if (Double.compare(part.power, power) != 0) return false;
        if (year != part.year) return false;
        if (timeCreated != part.timeCreated) return false;
        if (timeToLive != part.timeToLive) return false;
        if (newPart != part.newPart) return false;
        if (usedPart != part.usedPart) return false;
        if (id != null ? !id.equals(part.id) : part.id != null) return false;
        if (model != null ? !model.equals(part.model) : part.model != null) return false;
        if (category != null ? !category.equals(part.category) : part.category != null)
            return false;
        if (partName != null ? !partName.equals(part.partName) : part.partName != null)
            return false;
        if (chasis != null ? !chasis.equals(part.chasis) : part.chasis != null) return false;
        if (fuel != null ? !fuel.equals(part.fuel) : part.fuel != null) return false;
        if (userID != null ? !userID.equals(part.userID) : part.userID != null) return false;
        if (status != null ? !status.equals(part.status) : part.status != null) return false;
        if (path != null ? !path.equals(part.path) : part.path != null) return false;
        return images != null ? images.equals(part.images) : part.images == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id != null ? id.hashCode() : 0;
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (partName != null ? partName.hashCode() : 0);
        result = 31 * result + (chasis != null ? chasis.hashCode() : 0);
        temp = Double.doubleToLongBits(power);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (fuel != null ? fuel.hashCode() : 0);
        result = 31 * result + year;
        result = 31 * result + (userID != null ? userID.hashCode() : 0);
        result = 31 * result + (int) (timeCreated ^ (timeCreated >>> 32));
        result = 31 * result + (int) (timeToLive ^ (timeToLive >>> 32));
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (newPart ? 1 : 0);
        result = 31 * result + (usedPart ? 1 : 0);
        result = 31 * result + (images != null ? images.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Part{" +
                "id='" + id + '\'' +
                ", model='" + model + '\'' +
                ", category='" + category + '\'' +
                ", partName='" + partName + '\'' +
                ", chasis='" + chasis + '\'' +
                ", power=" + power +
                ", fuel='" + fuel + '\'' +
                ", year=" + year +
                ", userID='" + userID + '\'' +
                ", timeCreated=" + timeCreated +
                ", timeToLive=" + timeToLive +
                ", status='" + status + '\'' +
                ", path='" + path + '\'' +
                ", newPart=" + newPart +
                ", usedPart=" + usedPart +
                ", images=" + images +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(model);
        dest.writeString(category);
        dest.writeString(partName);
        dest.writeString(chasis);
        dest.writeDouble(power);
        dest.writeString(fuel);
        dest.writeInt(year);
        dest.writeString(userID);
        dest.writeLong(timeCreated);
        dest.writeLong(timeToLive);
        dest.writeString(status);
        dest.writeString(path);
        dest.writeByte((byte) (newPart ? 1 : 0));
        dest.writeByte((byte) (usedPart ? 1 : 0));
    }
}
