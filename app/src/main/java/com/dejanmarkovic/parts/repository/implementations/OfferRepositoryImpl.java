package com.dejanmarkovic.parts.repository.implementations;

import com.dejanmarkovic.parts.repository.Criteria;
import com.dejanmarkovic.parts.repository.OfferRepository;
import com.dejanmarkovic.parts.repository.entities.Offer;
import com.dejanmarkovic.parts.repository.providers.OfferDataProvider;
import java.util.List;

/**
 * Created by dejanmarkovic on 4/10/17.
 */

public class OfferRepositoryImpl implements OfferRepository {

    private OfferDataProvider offerDataProvider;

    public OfferRepositoryImpl(OfferDataProvider offerDataProvider) {

        this.offerDataProvider = offerDataProvider;
    }

    @Override
    public List<Offer> getOffers(Criteria criteria) {
        return offerDataProvider.getOffers(criteria);
    }

    @Override
    public void publishOffer(Offer offer) {
        offerDataProvider.publishOffer(offer);
    }
}
