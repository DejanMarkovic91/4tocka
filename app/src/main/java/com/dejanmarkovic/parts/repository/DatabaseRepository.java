package com.dejanmarkovic.parts.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dejanmarkovic.parts.repository.entities.Entity;
import com.dejanmarkovic.parts.repository.entities.TopicEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by dejanm on 27-Apr-17.
 */

public class DatabaseRepository extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "data.db";

    public DatabaseRepository(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void saveEntity(Entity entity) {
        upsertTicket(entity);
    }

    public boolean saveEntities(Collection<? extends Entity> entities) {
        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        boolean status = false;
        try {
            for (Entity entity : entities) {
                upsertTicket(entity);
            }
            database.setTransactionSuccessful();
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return status;
    }

    public boolean sync(List<Entity> entities) {
        SQLiteDatabase database = getWritableDatabase();
        database.beginTransaction();
        boolean status = false;
        try {
            for (Entity entity : entities) {
                upsertTicket(entity);
            }
            database.setTransactionSuccessful();
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            database.endTransaction();
        }
        return status;
    }

    public List<Entity> getEntitiesByQuery(Entity entity, SimpleQuery query) {
        List<Entity> entities = new ArrayList<>();
        Cursor cursor = getReadableDatabase().query(entity.getTableName(), null,
                query.getFormattedQuery(),
                null, null, null, null);
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    Entity e1 = (Entity) entity.clone();
                    e1.convertFromCursor(cursor);
                    entities.add(e1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }
        return entities;
    }

    private long insert(Entity entity) {
        ContentValues values = entity.populateContentValues();
        return getWritableDatabase().insertWithOnConflict(entity.getTableName(), null,
                values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    private int update(Entity entity) {
        ContentValues values = entity.populateContentValues();
        int update = getWritableDatabase().update(entity.getTableName(),
                values, "_id =" + entity.getId(), null);
        return update;
    }

    private void upsertTicket(Entity entity) {
        if (update(entity) == 0) {
            insert(entity);
        }
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TopicEntity.CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //do updating stuff
        onCreate(db);
    }
}
