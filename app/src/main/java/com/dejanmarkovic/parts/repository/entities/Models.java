package com.dejanmarkovic.parts.repository.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by dejanmarkovic on 4/25/17.
 */

public class Models implements Serializable {
    private String name;
    private ArrayList<String> models = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getModels() {
        return models;
    }

    public void setModels(ArrayList<String> models) {
        this.models = models;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Models models1 = (Models) o;

        if (name != null ? !name.equals(models1.name) : models1.name != null) return false;
        return models != null ? models.equals(models1.models) : models1.models == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (models != null ? models.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Models{" +
                "name='" + name + '\'' +
                ", models=" + models +
                '}';
    }
}
