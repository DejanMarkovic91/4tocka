package com.dejanmarkovic.parts.repository.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dejanmarkovic on 4/19/17.
 */

public class UserDetails {
    private String address;
    private String city;
    private String cityCode;
    private Company company;
    private String nameSurname;
    private String phone;
    private List<PartDescriptor> parts = new ArrayList<>();
    private List<ImageDescriptor> images = new ArrayList<>();

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<PartDescriptor> getParts() {
        return parts;
    }

    public void setParts(List<PartDescriptor> parts) {
        this.parts = parts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDetails that = (UserDetails) o;

        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (cityCode != null ? !cityCode.equals(that.cityCode) : that.cityCode != null)
            return false;
        if (company != null ? !company.equals(that.company) : that.company != null) return false;
        if (nameSurname != null ? !nameSurname.equals(that.nameSurname) : that.nameSurname != null)
            return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (parts != null ? !parts.equals(that.parts) : that.parts != null) return false;
        return images != null ? images.equals(that.images) : that.images == null;

    }

    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (cityCode != null ? cityCode.hashCode() : 0);
        result = 31 * result + (company != null ? company.hashCode() : 0);
        result = 31 * result + (nameSurname != null ? nameSurname.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (parts != null ? parts.hashCode() : 0);
        result = 31 * result + (images != null ? images.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", cityCode='" + cityCode + '\'' +
                ", company=" + company +
                ", nameSurname='" + nameSurname + '\'' +
                ", phone='" + phone + '\'' +
                ", parts=" + parts +
                ", images=" + images +
                '}';
    }
}
