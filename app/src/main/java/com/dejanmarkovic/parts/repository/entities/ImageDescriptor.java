package com.dejanmarkovic.parts.repository.entities;

/**
 * Created by dejanmarkovic on 4/19/17.
 */

public class ImageDescriptor {
    private String path;
    private long timeCreated;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.timeCreated = timeCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImageDescriptor that = (ImageDescriptor) o;

        if (timeCreated != that.timeCreated) return false;
        return path != null ? path.equals(that.path) : that.path == null;

    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (int) (timeCreated ^ (timeCreated >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "ImageDescriptor{" +
                "path='" + path + '\'' +
                ", timeCreated=" + timeCreated +
                '}';
    }
}
