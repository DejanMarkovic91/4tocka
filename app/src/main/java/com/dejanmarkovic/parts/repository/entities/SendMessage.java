package com.dejanmarkovic.parts.repository.entities;

import java.util.HashMap;

/**
 * Created by dejanm on 28-Apr-17.
 */

public class SendMessage {
    private String to;
    private HashMap<String, String> data = new HashMap<>();
    private long time_to_live;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public void setData(HashMap<String, String> data) {
        this.data = data;
    }

    public long getTime_to_live() {
        return time_to_live;
    }

    public void setTime_to_live(long time_to_live) {
        this.time_to_live = time_to_live;
    }
}
