package com.dejanmarkovic.parts.repository.entities;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by dejanm on 27-Apr-17.
 */

public class TopicEntity extends Entity {

    private static final String TABLE_NAME = "topics";
    private static final String TOPIC_NAME = "topic_name";
    private static final String STATUS = "status";
    public static final String CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + " (" + ID
            + " INTEGER PRIMARY KEY, " + TOPIC_NAME + " TEXT UNIQUE, "
            + STATUS + " TEXT "
            + " )";
    public static final String SUBSCRIBED = "subscribed";
    public static final String UNSUBSCRIBED = "unsubscribed";
    private long id;
    private String topicName;
    private String status;

    @Override
    public long getId() {
        return id;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTopicName() {
        return topicName;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public ContentValues populateContentValues() {
        ContentValues values = new ContentValues();
        values.put(ID, id);
        values.put(TOPIC_NAME, topicName);
        values.put(STATUS, status);
        return values;
    }

    @Override
    public void convertFromCursor(Cursor cursor) {
        id = cursor.getLong(cursor.getColumnIndex(ID));
        topicName = cursor.getString(cursor.getColumnIndex(TOPIC_NAME));
        status = cursor.getString(cursor.getColumnIndex(STATUS));
    }

    @Override
    public Object clone() {
        TopicEntity entity = new TopicEntity();
        entity.id = id;
        entity.topicName = topicName;
        entity.status = status;
        return entity;
    }
}
