package com.dejanmarkovic.parts.repository.entities;

import java.util.List;

/**
 * Created by dejanmarkovic on 4/19/17.
 */

public class Company {

    private List<Models> models;//null is all

    public Company() {
    }

    public Company(List<Models> models) {
        this.models = models;
    }

    public List<Models> getModels() {
        return models;
    }

    public void setModels(List<Models> models) {
        this.models = models;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Company company = (Company) o;

        return models != null ? models.equals(company.models) : company.models == null;

    }

    @Override
    public int hashCode() {
        return models != null ? models.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Company{" +
                "models=" + models +
                '}';
    }
}
