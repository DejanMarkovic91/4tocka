package com.dejanmarkovic.parts;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by dejanmarkovic on 4/14/17.
 */

public class StethoUtils{

    public static void install(Application application){
        Stetho.initializeWithDefaults(application);
    }
}
